{
 "cells": [
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Python Tips\n",
    "\n",
    "A collection of quick examples for writing great Python code."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 1,
   "metadata": {},
   "outputs": [],
   "source": [
    "import datetime\n",
    "\n",
    "import pandas as pd\n",
    "from IPython.core.interactiveshell import InteractiveShell\n",
    "\n",
    "InteractiveShell.ast_node_interactivity = 'last_expr_or_assign'"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Code Style: PEP 8\n",
    "\n",
    "There is a standard for Python code style: it's called [PEP 8](https://pep8.org/). Almost all Python code follows it; when everybody formats code the same way, it makes it easier to see the meaning without getting tripped up by superficial details.\n",
    "\n",
    "Here is a visual guide to the highlights.  First, indents are always 4 spaces (never tabs)."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "```python\n",
    "\"\"\"Python Style Visual Example\"\"\"\n",
    "\n",
    "import sys              # Import builtin modules first\n",
    "\n",
    "import pandas as pd     # Then third-party libraries\n",
    "\n",
    "import utils            # Then your own modules\n",
    "\n",
    "\n",
    "# Two spaces between module-level definitions\n",
    "PI = 3.14159        # Constants in CAPS\n",
    "\n",
    "\n",
    "# Everything else lower_case_with_underscores...\n",
    "def area_of_circle(radius):\n",
    "    \"\"\"Return the area of a circle of a given `radius`.\"\"\"\n",
    "    return PI * radius ** 2   # One space around operators\n",
    "\n",
    "\n",
    "class ExampleClass:     # ...Except ClassNames in CamelCase\n",
    "    \"\"\"Example of how classes look.\"\"\"\n",
    "    # No space around = in keyword args (only)\n",
    "    def method_one(self, arg, kwarg=None):\n",
    "        pass\n",
    "\n",
    "    # One space between things inside classes\n",
    "    def method_two(self):\n",
    "        # One space after items in lists and dicts\n",
    "        lists = [1, 2, 3, 4]\n",
    "        dicts = {1: 2, 3: 4, 5: 6}\n",
    "        return lists, dicts\n",
    "```"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "By far the simplest way to have great Python style is to have your editor highlight style issues in real time, and/or automatically reformat your code.  The [Hitchhiker's Guide To Python](https://docs.python-guide.org/dev/env/) lists methods and plugins for many popular editors; start there.\n",
    "\n",
    "In particular: PyCharm/IntelliJ has great builtin highlighting and [formatting](https://www.jetbrains.com/help/pycharm/reformat-and-rearrange-code.html); modern versions of Vim can show and fix formatting issues in real time with [ALE](https://github.com/dense-analysis/ale), and older versions can use [Syntastic](https://github.com/vim-syntastic/syntastic) or [vim-flake8](https://github.com/nvie/vim-flake8); and SublimeText has [SublimeLinter](https://github.com/SublimeLinter/SublimeLinter) + [pycodestyle](https://packagecontrol.io/packages/SublimeLinter-pycodestyle).\n",
    "\n",
    "If you can't catch and fix style issues with your editor, you can run [pycodestyle](https://pypi.org/project/pycodestyle/) to find issues in Python source files, and [autopep8](https://pypi.org/project/autopep8/) or [Black](https://black.readthedocs.io/en/stable/) to automatically fix them.  For bonus points, you can skip straight to [Pylint](https://pylint.org/) to find style problems _and_ bugs at the same time!"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## F-Strings\n",
    "\n",
    "Python 3.6 introduced a handy new method of string interpolation called f-strings.  Instead of the `.format()` method:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 2,
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "Hello, World!\n"
     ]
    }
   ],
   "source": [
    "name = 'World'\n",
    "print('Hello, {}!'.format(name))"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "You can prefix a string with `f`, and use variables and expressions right inside the string itself:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 3,
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "Hello, World!\n"
     ]
    }
   ],
   "source": [
    "print(f'Hello, {name}!')"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "All of the `format()` syntax you know and love works exactly the same:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 4,
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "PI = 3.14\n",
      "099\n",
      "100\n",
      "101\n",
      "2020-02-20\n"
     ]
    }
   ],
   "source": [
    "# Numerical precision\n",
    "PI = 3.14159\n",
    "print(f'PI = {PI:.2f}')\n",
    "\n",
    "# Zero padding\n",
    "for i in range(99, 102):\n",
    "    print(f'{i:03}')\n",
    "\n",
    "# Date formatting\n",
    "date = datetime.date(2020, 2, 20)\n",
    "print(f'{date:%Y-%m-%d}')"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Padding, truncating, left/right justifying, centering, thousands separators, etc. are all available.  [PyFormat](https://pyformat.info/) is a great visual reference to the many things you can do with Python string formatting.\n",
    "\n",
    "One of the best new features of f-strings in particular is expression evaluation inside brackets:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 5,
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "The area is 78.53975\n"
     ]
    }
   ],
   "source": [
    "radius = 5\n",
    "print(f'The area is {PI * radius ** 2}')"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "You can call functions, access dictionaries, and pretty much anything you can do in a normal python expression."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 6,
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "Dict contains 2 items\n",
      "a = 1\n"
     ]
    }
   ],
   "source": [
    "items = {'a': 1, 'b': 2}\n",
    "print(f'Dict contains {len(items)} items')\n",
    "print(f'a = {items[\"a\"]}')"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "And of course you can combine expressions with all the usual format specifiers:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 7,
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "data has mean 4.50, std 3.03\n"
     ]
    }
   ],
   "source": [
    "data = pd.Series(range(10))\n",
    "print(f'data has mean {data.mean():.2f}, std {data.std():.2f}')"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "One nice bonus is that f-strings are actually faster than the `format()` method, due to special interpreter support:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 8,
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "634 ns ± 96.1 ns per loop (mean ± std. dev. of 7 runs, 1000000 loops each)\n"
     ]
    }
   ],
   "source": [
    "%timeit 'PI = {:.2f}!'.format(PI)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 9,
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "474 ns ± 71.7 ns per loop (mean ± std. dev. of 7 runs, 1000000 loops each)\n"
     ]
    }
   ],
   "source": [
    "%timeit f'PI = {PI:.2f}!'"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Python 3.8 introduces support for making debug print statements much more convenient.  Instead of:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 10,
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "len(data) = 10\n"
     ]
    }
   ],
   "source": [
    "print(f'len(data) = {len(data)}')"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "You can just put your expression in brackets with the `=` format specifier, and it will print the expression followed by `=` and the value:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 11,
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "len(data) = 10\n"
     ]
    }
   ],
   "source": [
    "print(f'{len(data) = }' )"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Python f-strings are a handy timesaver and string-formatting workhorse.  You can find all the details in the \n",
    "[Format Specification Mini-Language](https://docs.python.org/3/library/string.html#formatspec) reference."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Tricks with Dicts"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Dictionaries are one of the most powerful and useful features of Python.  Mastering their power will make your code simple, fast, and easy to write and read."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Creation: Literals"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Dictionaries can be created with literal curly-braces syntax or with the [dict()](https://docs.python.org/3/library/stdtypes.html#dict) constructor; use whichever is more convenient, keeping in mind `dict()` can only handle string keys which are valid [identifiers](https://docs.python.org/3/reference/lexical_analysis.html#identifiers) (variable names)."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 12,
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/plain": [
       "{'a': 1, 'b': 2, 'c': 3}"
      ]
     },
     "execution_count": 12,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "d = {'a': 1, 'b': 2, 'c': 3}\n",
    "d = dict(a=1, b=2, c=3)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Don't create an empty dict and then manually assign each element; this is needlessly verbose, slow, and [repeats yourself](https://en.wikipedia.org/wiki/Don%27t_repeat_yourself)."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 13,
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "177 ns ± 11.9 ns per loop (mean ± std. dev. of 7 runs, 1000000 loops each)\n"
     ]
    }
   ],
   "source": [
    "%%timeit\n",
    "d = {}   # Making dicts this way is verbose, repetetive, and slow\n",
    "d['a'] = 1\n",
    "d['b'] = 2\n",
    "d['c'] = 3"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 14,
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "145 ns ± 10 ns per loop (mean ± std. dev. of 7 runs, 10000000 loops each)\n"
     ]
    }
   ],
   "source": [
    "%timeit d = {'a': 1, 'b': 2, 'c': 3}"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "You can of course also create a dict from an iterable of `(key, value)` tuples."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 15,
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/plain": [
       "{'a': 1, 'b': 2, 'c': 3}"
      ]
     },
     "execution_count": 15,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "items = [('a', 1), ('b', 2), ('c', 3)]\n",
    "dict(items)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Comprehensions"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Dict comprehensions are often the best way to make a dictionary.  They support all the same constructions as list comprehensions, including filtering and nested loops.  Comprehensions are always preferable to creating an empty data structure and looping to insert things (this goes for lists and sets, too!)."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 16,
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/plain": [
       "{0: 0, 1: 1, 2: 4, 3: 9, 4: 16, 5: 25}"
      ]
     },
     "execution_count": 16,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "d = {x: x ** 2 for x in range(6)}"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 17,
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/plain": [
       "{0: 0, -2: 4, -4: 16}"
      ]
     },
     "execution_count": 17,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "d = {-k: v for k, v in d.items() if k % 2 == 0}"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Order"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Since Python 3.6, dictionaries are ordered: iterating gives the keys and values in the order they were first inserted (*not* sorted order).  This comes in handy more often than you'd think, for e.g., maintaining the order of columns in a table, event logging, deterministic / diff-able output, comparison with ordered collections, etc.  Ordering holds however you create the dict: literals you type in your source code, the `dict()` constructor, comprehensions, or assignment."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 18,
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "{'first': 'breakfast', 'middle': 'lunch', 'last': 'dinner'}\n"
     ]
    }
   ],
   "source": [
    "d = dict(first='breakfast', middle='lunch', last='dinner')\n",
    "print(d)\n",
    "assert list(d.keys()) == ['first', 'middle', 'last']\n",
    "assert list(d.values()) == ['breakfast', 'lunch', 'dinner']"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The order does not change if you reassign an existing key."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 19,
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "{'first': 'breakfast', 'middle': 'brunch', 'last': 'dinner'}\n"
     ]
    }
   ],
   "source": [
    "d['middle'] = 'brunch'    # Existing key: does not change order\n",
    "print(d)   "
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 20,
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "{'first': 'breakfast', 'middle': 'brunch', 'last': 'dinner', 'next': 'snack'}\n"
     ]
    }
   ],
   "source": [
    "d['next'] = 'snack'       # New key: goes last\n",
    "print(d)   "
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "If you want to change the order, `del`-eting and reinserting puts an entry at the end."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 21,
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "{'first': 'breakfast', 'middle': 'brunch', 'next': 'snack', 'last': 'dinner'}\n"
     ]
    }
   ],
   "source": [
    "del d['last']             # Deleting and reinserting sends key to the end\n",
    "d['last'] = 'dinner'\n",
    "print(d)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Missing and Default Values"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "You can of course use `get()` to retreive a value, or a default if it's missing.  "
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "To get a value or insert a default if it's not there, use [setdefault()](https://docs.python.org/3/library/stdtypes.html#dict.setdefault).  It's like `get()`, but also inserts a value if it's not there.  It's  useful for grouping or counting."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 22,
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "{'T': 1, 'h': 1, 'e': 1, ' ': 3, 'f': 1, 'a': 3, 't': 3, 'c': 1, 's': 1}\n"
     ]
    }
   ],
   "source": [
    "letters = 'The fat cat sat'\n",
    "count = {}\n",
    "for let in letters:\n",
    "    # If let is in the dict, return its value; otherwise, insert and return 0\n",
    "    count[let] = count.setdefault(let, 0) + 1\n",
    "print(count)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "For the specific case of counting, Python has an even more convenient and efficient way: [Counter](https://docs.python.org/3/library/collections.html?highlight=counter#collections.Counter).  Counters also have convenient operations for finding the most common elements and merging counts."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 23,
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "Counter({' ': 3, 'a': 3, 't': 3, 'T': 1, 'h': 1, 'e': 1, 'f': 1, 'c': 1, 's': 1})\n"
     ]
    }
   ],
   "source": [
    "from collections import Counter\n",
    "\n",
    "print(Counter(letters))"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "[defaultdict](https://docs.python.org/3/library/collections.html#collections.defaultdict) is a dictionary that automatically inserts and returns a default value of your choice if a key isn't present.  It's great for grouping by keys that you don't know ahead of time.\n",
    "\n",
    "You pass `defaultdict()` a function that returns a new default value.  For instance, `defaultdict(list)` will automatically call `list()` to create a new empty list for any key not in the dictionary."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 24,
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/plain": [
       "defaultdict(list,\n",
       "            {'T': ['The'],\n",
       "             'h': ['The'],\n",
       "             'e': ['The'],\n",
       "             'f': ['fat'],\n",
       "             'a': ['fat', 'cat', 'sat'],\n",
       "             't': ['fat', 'cat', 'sat'],\n",
       "             'c': ['cat'],\n",
       "             's': ['sat']})"
      ]
     },
     "execution_count": 24,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "from collections import defaultdict\n",
    "\n",
    "# Map each letter to the words that contain it\n",
    "dd = defaultdict(list)\n",
    "for word in letters.split():\n",
    "    for let in word:\n",
    "        dd[let].append(word)\n",
    "dd"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "One important warning with defaultdicts: *merely trying to access a key will insert it*.  This is very different from normal dicts (which throw a `KeyError`), and can cause problems in code not expecting it.  If you need to return a defaultdict, call `dict()` on it to copy it into a regular dictionary first."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Views"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The `keys()` and `values()` methods return views, which are set- or list-like objects that update when the dictionary does.  `keys()` in particular is useful because it supports all of the `set` operations like intersection, union, etc.  Views are efficient because they do not make copies."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 25,
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/plain": [
       "{'b', 'c'}"
      ]
     },
     "execution_count": 25,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "d1 = dict(a=1, b=2, c=3)\n",
    "d2 = dict(b=4, c=5, d=6)\n",
    "d1.keys() & d2.keys()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Merging"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Occasionally you need to merge two dicts, updating the items in one with the items from another.  There are several ways to do this.  The `update()` method modifies the dictionary in-place, adding to or overwriting its items with items from the second dict."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 26,
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/plain": [
       "{'a': 1, 'b': 4, 'c': 5, 'd': 6}"
      ]
     },
     "execution_count": 26,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "d1.update(d2)\n",
    "d1"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "If you want to create and return a new dict without modifying the original -- *and you only update string keys* -- you can use this \"hack,\" turning the second dict into keyword arguments for the `dict()` constructor, overriding anything in the first:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 27,
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/plain": [
       "{'a': 1, 'b': 4, 'c': 5, 'd': 6}"
      ]
     },
     "execution_count": 27,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "d1 = dict(a=1, b=2, c=3)\n",
    "d2 = dict(b=4, c=5, d=6)\n",
    "dict(d1, **d2)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "This copies the dicts; they are not modified.\n",
    "\n",
    "Python 3.5 introduced generalizations to the `**` unpacking operator, which can be used for merging any number of dicts (this also makes copies):"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 28,
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/plain": [
       "{'a': 1, 'b': 4, 'c': 5, 'd': 6}"
      ]
     },
     "execution_count": 28,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "{**d1, **d2}"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Finally, in Python 3.9, you can use the good old set union operator `|`.  It also has an update form `|=`."
   ]
  },
  {
   "cell_type": "raw",
   "metadata": {},
   "source": [
    "d1 | d2\n",
    "{'a': 1, 'b': 4, 'c': 5, 'd': 6}"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": []
  },
  {
   "cell_type": "code",
   "execution_count": 29,
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/html": [
       "<!-- Hide prompts and fix width -->\n",
       "<style>\n",
       "div.prompt { display:none }\n",
       ".container { width:66em !important; }\n",
       "</style>\n"
      ],
      "text/plain": [
       "<IPython.core.display.HTML object>"
      ]
     },
     "metadata": {},
     "output_type": "display_data"
    }
   ],
   "source": [
    "%%HTML\n",
    "<!-- Hide prompts and fix width -->\n",
    "<style>\n",
    "div.prompt { display:none }\n",
    ".container { width:66em !important; }\n",
    "</style>"
   ]
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "Python 3",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.8.3"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 4
}
